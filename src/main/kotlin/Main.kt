package io.github.zerumi

import kotlin.random.Random

val ways = arrayOf(
    arrayOf(0, 4, 5, 3, 8),
    arrayOf(4, 0, 7, 6, 8),
    arrayOf(5, 7, 0, 7, 9),
    arrayOf(3, 6, 7, 0, 9),
    arrayOf(8, 8, 9, 9, 0),
)

const val POPULATION_SIZE = 4
const val CITIES_SIZE = 5
const val ITERATIONS = 100

data class Chromosome(
    val cities: List<Int>
) {
    fun getTargetFunction(): Int {
        var result = 0
        for (i in cities.indices) {
            val fromCity = cities[if (i == 0) cities.size - 1 else i - 1]
            val toCity = cities[i]
            result += ways[fromCity][toCity]
        }
        return result
    }
}

fun main() {
    var i = 1
    val citiesBypassSheet = generateSequence { i++ }.take(CITIES_SIZE - 1).toList()


    val population = mutableListOf(
        Chromosome(listOf(0).plus(citiesBypassSheet)),
    )

    repeat(POPULATION_SIZE - 1) {
        population.add(
            Chromosome(listOf(0).plus(citiesBypassSheet.shuffled())),
        )
    }

    repeat(ITERATIONS) {
        // скрещивание
        val parents = population.shuffled().take(4)

        val breakPoint = Random.nextInt(CITIES_SIZE)

        for (j in parents.indices step 2) {
            val firstParent = parents[j]
            val secondParent = parents[j + 1]

            var firstChild = firstParent.cities.take(breakPoint).toList()
            firstChild =
                firstChild.plus(
                    secondParent.cities
                        .drop(breakPoint)
                        .filter { it !in firstChild }
                )
            firstChild =
                firstChild.plus(
                    secondParent.cities
                        .take(breakPoint)
                        .filter { it !in firstChild }
                )

            var secondChild = secondParent.cities.take(breakPoint).toList()
            secondChild =
                secondChild.plus(
                    firstParent.cities
                        .drop(breakPoint)
                        .filter { it !in secondChild }
                )
            secondChild =
                secondChild.plus(
                    firstParent.cities
                        .take(breakPoint)
                        .filter { it !in secondChild }
                )

            // мутация
            val chanceTest = Random.nextInt(100)
            if (chanceTest == 0) {
                var index1: Int
                var index2: Int

                do {
                    index1 = Random.nextInt(CITIES_SIZE)
                    index2 = Random.nextInt(CITIES_SIZE)
                } while (index1 == index2)

                val swappedChild = firstChild.toMutableList()

                swappedChild[index1] = swappedChild[index2].also { swappedChild[index2] = swappedChild[index1] }

                firstChild = swappedChild
            }

            // добавляем в популяцию
            population.add(Chromosome(firstChild))
            population.add(Chromosome(secondChild))
        }

        population.sortBy { it.getTargetFunction() }

        repeat(POPULATION_SIZE) { population.removeLast() }
    }

    println("Result: ${population[0].cities.map { it + 1 }} Sum: ${population[0].getTargetFunction()}")
}
